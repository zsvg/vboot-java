package vboot.core.common.mvc.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class ZidNamePid {

    private String id;

    private String name;

    private String pid;
}
