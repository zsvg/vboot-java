package vboot.core.common.mvc.pojo;

import lombok.Data;

@Data
public class ZidOrnum {

    private String id;

    private Integer ornum;
}
