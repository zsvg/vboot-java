package vboot.core.framework.security.pojo;

import lombok.Data;

import java.io.Serializable;

//前台返回菜单meta信息
@Data
public class Zmeta implements Serializable {
    private static final long serialVersionUID = 1L;

    private String title;

    private Boolean isHide;

    private Boolean isIframe=false;

    private String isLink;

    private String affix;

    private String icon;

    private Integer orderNo;

    private Boolean isKeepAlive;

}
