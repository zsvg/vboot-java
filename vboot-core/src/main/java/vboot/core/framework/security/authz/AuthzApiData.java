package vboot.core.framework.security.authz;

import vboot.core.framework.security.pojo.Yapi;

import java.util.ArrayList;
import java.util.List;

//所有需要认证API(URL)的内存快照，根据请求method分类，用于快速找到请求URL的权限码
public class AuthzApiData {

    public static List<Yapi> GET1_APIS = new ArrayList<>();

    public static List<Yapi> GET2_APIS = new ArrayList<>();

    public static List<Yapi> POST_APIS = new ArrayList<>();

    public static List<Yapi> PUT_APIS = new ArrayList<>();

    public static List<Yapi> DELETE_APIS = new ArrayList<>();

    public static List<Yapi> PATCH_APIS = new ArrayList<>();

    public static int AUTHPOS = 0;
}
