package vboot.core.framework.webservice;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


//https://blog.csdn.net/m0_50172764/article/details/126014196
//https://blog.csdn.net/m0_48958478/article/details/125274698
/**
 * <b>功能描述:CXF配置类</b><br>
 *
 * @author newzhong
 * @version 1.0.0
 * @since JDK 1.8
 */
@Configuration
public class CxfConfig {
    @Bean(name = Bus.DEFAULT_BUS_ID)
    public SpringBus springBus() {
        SpringBus bus = new SpringBus();
//        bus.getFeatures().add(new LoggingFeature());
        return bus;
    }
}