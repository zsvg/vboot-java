package vboot.core.module.gen.org;

import io.swagger.annotations.Api;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.utils.web.XuserUtil;
import vboot.core.module.sys.org.rece.SysOrgRece;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("gen/org/rece")
@Api(tags = {"组织架构服务"})
public class GenOrgReceApi {

    @GetMapping
    public R get(Integer type) {
        List<Map<String, Object>> mapList = new ArrayList<>();
        String userId = XuserUtil.getUserId();
        if ((type & 8) != 0) {//用户
            Sqler userSqler = new Sqler("t.orgid as id", "sys_org_rece");
            userSqler.addInnerJoin("u.name", "sys_org_user u", "u.id=t.orgid");
            userSqler.addInnerJoin("d.name as dept", "sys_org_dept d", "d.id=u.depid");
            userSqler.addEqual("t.useid", userId);
            mapList.addAll(jdbcDao.findMapList(userSqler));
        }
        if ((type & 2) != 0||(type & 1) != 0) {//部门
            Sqler deptSqler = new Sqler("t.orgid as id", "sys_org_rece");
            deptSqler.addInnerJoin("o.name", "sys_org_dept o", "o.id=t.orgid");
            deptSqler.addEqual("t.useid", userId);
            mapList.addAll(jdbcDao.findMapList(deptSqler));
        }
        if ((type & 4) != 0) {//岗位
            Sqler postSqler = new Sqler("t.orgid as id", "sys_org_rece");
            postSqler.addInnerJoin("p.name", "sys_org_post p", "p.id=t.orgid");
            postSqler.addInnerJoin("d.name as dept", "sys_org_dept d", "d.id=p.depid");
            postSqler.addEqual("t.useid", userId);
            mapList.addAll(jdbcDao.findMapList(postSqler));
        }
//        sqler.addDescOrder("t.uptim");
        return R.ok(mapList);
    }

    @PostMapping
    public R post(@RequestBody List<SysOrgRece> reces) {
        if(reces!=null&&reces.size()>0){
            service.update(reces);
        }
        return R.ok();
    }


    @Autowired
    private GenOrgReceService service;

    @Autowired
    private JdbcDao jdbcDao;


}
