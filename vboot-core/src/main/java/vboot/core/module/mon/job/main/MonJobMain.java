package vboot.core.module.mon.job.main;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.Date;

@Data
@Entity
@ApiModel("定时任务信息")
public class MonJobMain
{
    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 128, updatable = false)
    @ApiModelProperty("名称")
    private String name;

    @Column(length = 128, updatable = false)
    @ApiModelProperty("代码")
    private String code;

//    @Column(length = 32, updatable = false)
    @Transient
    private String jid;
//
//    @Column(length = 32, updatable = false)
    @Transient
    private String jgroup;

    @ApiModelProperty("请求类型")
    private Integer retyp;

    @Column(length = 32, updatable = false)
    @ApiModelProperty("请求URL")
    private String reurl;

    @ApiModelProperty("是否可用")
    private Boolean avtag;

    @ApiModelProperty("排序号")
    private Integer ornum;

    @Column(updatable = false)
    @ApiModelProperty("创建时间")
    private Date crtim = new Date();

    @Column(length = 32)
    @ApiModelProperty("执行表达式")
    private String cron;

    @ApiModelProperty("备注")
    private String notes;

}
