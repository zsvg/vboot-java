package vboot.core.module.mon.server.main;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vboot.core.common.mvc.api.R;


@RestController
@RequestMapping("mon/server/main")
@Api(tags = {"服务器监控"})
public class MonServerMainApi {

    @Autowired
    private MonitorServiceImpl monitorService;

    @GetMapping
    @ApiOperation("查询服务器监控信息")
    public R get() throws Exception {
        MonServerMain server = new MonServerMain();
        server.copyTo();
//        return R.ok(monitorService.getServers());
        return R.ok(server);
    }

}
