package vboot.core.module.mon.log.oper;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MonLogOperRepo extends JpaRepository<MonLogOper, String>, JpaSpecificationExecutor<MonLogOper> {

}
