package vboot.core.module.ass.addr.pojo;

import lombok.Data;

import java.util.List;

@Data
public class DistrictProv {

    private String adcode;

    private String name;

    private String center;

    private String level;

    private List<District> districts;

}
