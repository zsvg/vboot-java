package vboot.core.module.ass.addr.pojo;

import lombok.Data;

@Data
public class Ares {
    private int status;

    private String info;

    private int count;

    private Aroute route;

    private int dist;


}
