package vboot.core.module.ass.addr.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vboot.core.common.mvc.service.BaseService;

import javax.annotation.PostConstruct;

@Service
public class AssAddrMainService extends BaseService<AssAddrMain> {

    //----------bean注入------------
    @Autowired
    private AssAddrMainRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }

}