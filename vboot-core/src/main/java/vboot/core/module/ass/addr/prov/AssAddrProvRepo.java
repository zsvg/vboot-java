package vboot.core.module.ass.addr.prov;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AssAddrProvRepo extends JpaRepository<AssAddrProv, String> {

}