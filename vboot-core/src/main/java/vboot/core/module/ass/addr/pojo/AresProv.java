package vboot.core.module.ass.addr.pojo;

import lombok.Data;

import java.util.List;

@Data
public class AresProv {

    private int status;

    private String info;

    private int count;

    private Aroute route;

    private List<DistrictProv> districts;

}
