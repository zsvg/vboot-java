package vboot.core.module.bpm.node.main;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Getter
@Setter
@ApiModel("流程节点信息")
public class BpmNodeMain {

    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 32)
    @ApiModelProperty("当前节点编号:N1,N2")
    private String facno;

    @Column(length = 126)
    @ApiModelProperty("当前节点名称")
    private String facna;

    @Column(length = 32)
    @ApiModelProperty("当前节点类型")
    private String facty;

    @Column(length = 32)
    @ApiModelProperty("流转方式")
    private String flway;

    @Column(length = 32)
    @ApiModelProperty("流程实例id")
    private String proid;

    @Column(length = 8)
    @ApiModelProperty("状态")
    private String state;

    @Column(updatable = false)
    @ApiModelProperty("开始时间")
    protected Date sttim = new Date();

}