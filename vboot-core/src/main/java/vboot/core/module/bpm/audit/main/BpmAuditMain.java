package vboot.core.module.bpm.audit.main;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@ApiModel("流程评审信息")
public class BpmAuditMain {

    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 32)
    @ApiModelProperty("当前节点ID:N1,N2")
    private String facno;

    @Column(length = 126)
    @ApiModelProperty("当前节点名称")
    private String facna;

    @Column(updatable = false)
    @ApiModelProperty("开始时间")
    protected Date crtim = new Date();

    @Column(length = 32)
    @ApiModelProperty("流程id")
    private String proid;

    @Column(length = 32)
    @ApiModelProperty("节点id")
    private String nodid;

    @Column(length = 32)
    @ApiModelProperty("任务id")
    private String tasid;

    @Column(length = 32)
    @ApiModelProperty("实处理人")
    private String haman;

//    @Column(length = 32)
//    private String auman;//授权处理人
//
//    @Column(length = 32)
//    private String exman;//应处理人

    @Column(length = 32)
    @ApiModelProperty("操作的key：pass，refuse")
    private String opkey;

    @Column(length = 64)
    @ApiModelProperty("操作的名称: 通过，驳回，转办等")
    private String opinf;

    @Column(length = 1000)
    @ApiModelProperty("审核留言：具体写了什么审核内容")
    private String opnot;

    @Column(length = 512)
    @ApiModelProperty("审核附件IDS")
    private String atids;

//    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
//    @JoinColumn(name = "audid")
//    @OrderBy("ornum ASC")
//    @ApiModelProperty("附件清单")
//    private List<BpmAuditAtt> atts = new ArrayList<>();


}