package vboot.core.module.bpm.task.main;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BpmTaskMainRepo extends JpaRepository<BpmTaskMain,String> {

    List<BpmTaskMain> findAllByProidAndActagOrderByOrnum(String proid,Boolean actag);

    List<BpmTaskMain> findAllByProidOrderByOrnum(String proid);

    void deleteAllByProid(String proid);
}