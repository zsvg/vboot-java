package vboot.core.module.bpm.proc.temp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import vboot.core.common.mvc.entity.BaseMainEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Lob;

@Entity
@Getter
@Setter
@ApiModel("流程模板信息")
public class BpmProcTemp extends BaseMainEntity {

    public BpmProcTemp() {

    }

    public BpmProcTemp(BaseMainEntity main) {
        this.name=main.getName();
        this.crman=main.getCrman();
        this.crtim=main.getCrtim();
    }

    @Lob
    @ApiModelProperty("原始XML")
    private String orxml;

    @Lob
    @ApiModelProperty("变动后的XML")
    private String chxml;

}