package vboot.core.module.bpm.proc.main;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import vboot.core.common.mvc.entity.BaseMainEntity;
import vboot.core.module.sys.org.root.SysOrg;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
@ApiModel("流程实例信息")
public class BpmProcMain extends BaseMainEntity {


    @Column(length = 32)
    @ApiModelProperty("待用字段")
    private String modid;

    @ApiModelProperty("待用字段")
    private String modty;


    @Column(length = 32)
    @ApiModelProperty("模板ID")
    private String temid;

    @Column(length = 8)
    @ApiModelProperty("状态")
    private String state;

    public BpmProcMain() {

    }

    public BpmProcMain(Zbpm zbpm) {
        this.id=zbpm.getProid();
        this.name=zbpm.getProna();
        this.temid=zbpm.getTemid();
        this.crman=new SysOrg(zbpm.getHaman());
    }

}