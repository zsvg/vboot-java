package vboot.core.module.sys.coop.user;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.module.sys.org.user.PacodPo;

@RestController
@RequestMapping("sys/coop/user")
@Api(tags = {"外部协同-外部用户"})
public class SysCoopUserApi {

    @GetMapping
    @ApiOperation("查询分页")
    public R get(String name,String corid) {
        Sqler sqler = new Sqler("sys_coop_user");
        sqler.addLike("t.name" , name);
        sqler.addEqual("t.corid" , corid);
        sqler.addLeftJoin("c.name corna","sys_coop_corp c" , "c.id=t.corid");
        sqler.addOrder("t.ornum");
        sqler.addSelect("t.notes,t.type");
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("list")
    @ApiOperation("查询列表")
    public R getList() {
        return R.ok(service.findAll());
    }


    @GetMapping("one/{id}")
    @ApiOperation("查询详情")
    public R getOne(@PathVariable String id) {
        SysCoopUser main = service.findOne(id);

        return R.ok(main);
    }

    @PostMapping
    @ApiOperation("新增")
    public R post(@RequestBody SysCoopUser main) {
        return R.ok(service.insertx(main));
    }

    @PutMapping
    @ApiOperation("修改")
    public R put(@RequestBody SysCoopUser main) {
        return R.ok(service.updatex(main));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.deletex(ids));
    }

    @PostMapping("pacod")
    @ApiOperation("修改密码")
    public R pacod(@RequestBody PacodPo po) {
        service.pacod(po);
        return R.ok();
    }

    @Autowired
    private SysCoopUserService service;

}
