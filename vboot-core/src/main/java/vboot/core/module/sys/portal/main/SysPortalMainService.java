package vboot.core.module.sys.portal.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vboot.core.common.mvc.service.BaseMainService;

import javax.annotation.PostConstruct;

@Service
public class SysPortalMainService extends BaseMainService<SysPortalMain> {


    //----------bean注入------------
    @Autowired
    private SysPortalMainRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }

}
