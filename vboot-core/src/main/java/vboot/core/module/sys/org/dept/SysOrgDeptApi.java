package vboot.core.module.sys.org.dept;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.pojo.TreeMovePo;
import vboot.core.common.utils.lang.TreeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("sys/org/dept")
@Api(tags = {"组织架构-部门管理"})
public class SysOrgDeptApi {

    private String table = "sys_org_dept";

    @GetMapping
    @ApiOperation("查询部门分页")
    public R get(String name, String pid, String notes) {
        Sqler sqler = new Sqler(table);
        if (StrUtil.isNotBlank(name)) {
            sqler.addLike("t.name" , name);
        } else {
            if ("".equals(pid)) {
                sqler.addWhere("t.pid is null");
            } else if (StrUtil.isNotBlank(pid)) {
                sqler.addEqual("t.pid" , pid);
            }
        }
        sqler.addLike("t.notes" , notes);
        sqler.addWhere("t.avtag=1");
        sqler.addOrder("t.ornum");
        sqler.addSelect("t.ornum,t.notes,t.pid,t.crtim,t.uptim");
        return R.ok(jdbcDao.findPageData(sqler));
    }

    @GetMapping("tree")
    public R getTree(String name) {
        Sqler sqler = new Sqler("sys_org_dept");
        sqler.addSelect("t.pid,t.notes,t.crtim,t.uptim,t.ornum");
        sqler.addLike("t.name", name);
        sqler.addOrder("t.ornum");
        sqler.addWhere("t.avtag=1");
        List<SysOrgDeptVo> list = jdbcDao.getTp().query(sqler.getSql(), sqler.getParams(),
                new BeanPropertyRowMapper<>(SysOrgDeptVo.class));
        return R.ok(TreeUtils.buildEtree(list));
    }


    //排除了自己
//    @GetMapping("listn")
//    public R listn(String name, String id) {
//        Sqler sqler = new Sqler(table);
//        sqler.addLike("t.name", name);
//        sqler.addWhere("t.avtag=1");
//        sqler.addOrder("t.ornum");
//        sqler.addSelect("t.pid");
//        return R.ok(service.findWithoutItself(sqler, id));
//    }

    @GetMapping("one/{id}")
    @ApiOperation("查询部门详情")
    public R getOne(@PathVariable String id, HttpServletRequest request) {
        SysOrgDept main = service.findById(id);
        return R.ok(main);
    }


    @PostMapping
    @ApiOperation("新增部门")
    public R post(@RequestBody SysOrgDept main) {
        service.insert(main);
        return R.ok(main.getId());
    }

    @PutMapping
    @ApiOperation("修改部门")
    public R put(@RequestBody SysOrgDept main) throws Exception {
        service.update(main);
        return R.ok(main.getId());
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除部门")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }


    @PostMapping("move")
    @ApiOperation("移动部门")
    public R move(@RequestBody TreeMovePo po) throws Exception {
        service.move(po);
        return R.ok();
    }

    @Autowired
    private JdbcDao jdbcDao;

    @Autowired
    private SysOrgDeptService service;

}
