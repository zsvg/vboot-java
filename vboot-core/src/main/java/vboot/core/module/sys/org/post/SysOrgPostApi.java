package vboot.core.module.sys.org.post;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.api.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("sys/org/post")
@Api(tags = {"组织架构-岗位管理"})
public class SysOrgPostApi {

    private String table = "sys_org_post";

    @GetMapping
    @ApiOperation("查询岗位分页")
    public R get(String depid, String name) {
        Sqler sqler = new Sqler(table);
        sqler.addEqual("t.depid", depid);
        sqler.addLike("t.name", name);
        sqler.addDescOrder("t.crtim");
        sqler.addSelect("t.crtim,t.uptim,t.depid,t.notes");
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询岗位详情")
    public R getOne(@PathVariable String id) {
        SysOrgPost main = service.findById(id);
        return R.ok(main);
    }

    @PostMapping
    @ApiOperation("新增岗位")
    public R post(@RequestBody SysOrgPost main) throws InterruptedException {
        return R.ok(service.insert(main));
    }

    @PutMapping
    @ApiOperation("修改岗位")
    public R put(@RequestBody SysOrgPost main) {
        return R.ok(service.update(main));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除岗位")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private SysOrgPostService service;

}
