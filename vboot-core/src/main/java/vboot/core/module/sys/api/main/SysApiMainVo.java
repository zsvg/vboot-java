package vboot.core.module.sys.api.main;

import lombok.Data;

@Data
public class SysApiMainVo {
    private String id;

    private String pid;
}
