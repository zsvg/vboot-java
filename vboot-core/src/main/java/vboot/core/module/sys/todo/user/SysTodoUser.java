package vboot.core.module.sys.todo.user;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@ApiModel("用户待办记录")
public class SysTodoUser {

    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 32)
    @ApiModelProperty("待办ID")
    private String todid;

    @Column(length = 32)
    @ApiModelProperty("用户ID")
    private String useid;
}
