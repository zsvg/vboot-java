package vboot.core.module.sys.portal.main;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;

@RestController
@RequestMapping("sys/portal/main")
@Api(tags = {"门户管理-门户信息"})
public class SysPortalMainApi {

    @GetMapping
    @ApiOperation("查询门户分页")
    public R get(String name) {
        Sqler sqler = new Sqler("sys_portal_main");
        sqler.addLike("t.name" , name);
        sqler.addOrder("t.ornum");
        sqler.addSelect("t.notes");
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("list")
    @ApiOperation("查询门户列表")
    public R getList() {
        return R.ok(service.findAll());
    }


    @GetMapping("one/{id}")
    @ApiOperation("查询门户详情")
    public R getOne(@PathVariable String id) {
        SysPortalMain main = service.findOne(id);
        return R.ok(main);
    }

    @PostMapping
    @ApiOperation("新增门户")
    public R post(@RequestBody SysPortalMain main) {
        return R.ok(service.insert(main));
    }

    @PutMapping
    @ApiOperation("修改门户")
    public R put(@RequestBody SysPortalMain main) {
        return R.ok(service.update(main));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除门户")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private SysPortalMainService service;

}
