package vboot.core.module.sys.org.group;

import cn.hutool.core.util.StrUtil;
import vboot.core.module.sys.org.root.SysOrg;
import vboot.core.module.sys.org.root.SysOrgRepo;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.api.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vboot.core.common.utils.lang.IdUtils;

import java.util.Date;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class SysOrgGroupService {

    @Transactional(readOnly = true)
    public SysOrgGroup findById(String id) {
        return groupRepo.findById(id).get();
    }

    public PageData findPageData(Sqler sqler) {
        return jdbcDao.findPageData(sqler);
    }

    public String insert(SysOrgGroup main) {
        if (main.getId() == null || "".equals(main.getId())) {
            main.setId(IdUtils.getUID());
        }
        SysOrg sysOrg = new SysOrg(main.getId(), main.getName(),16);
        orgRepo.save(sysOrg);
        groupRepo.save(main);
        return main.getId();
    }


    public String update(SysOrgGroup main) {
        main.setUptim(new Date());
        SysOrg sysOrg = new SysOrg(main.getId(), main.getName(),16);
        orgRepo.save(sysOrg);
        groupRepo.save(main);
        return main.getId();
    }

    public int delete(String[] ids) {
        for (String id : ids) {
            groupRepo.deleteById(id);
            orgRepo.deleteById(id);
        }
        return ids.length;
    }


    public List<SysOrgGroup> findAll(String name) {
        List<SysOrgGroup> list;
        if (StrUtil.isNotBlank(name)) {
            list = groupRepo.findByNameLikeOrderByOrnum("%" + name + "%");
        } else {
            list = groupRepo.findByOrderByOrnum();
        }
        return list;
    }

    @Autowired
    private JdbcDao jdbcDao;

    @Autowired
    private SysOrgGroupRepo groupRepo;

    @Autowired
    private SysOrgRepo orgRepo;

}
