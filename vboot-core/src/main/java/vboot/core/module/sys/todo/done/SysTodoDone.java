package vboot.core.module.sys.todo.done;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Getter
@Setter
@ApiModel("待办完成信息")
public class SysTodoDone {

    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 32)
    @ApiModelProperty("待办id")
    private String todid;

    @Column(length = 32)
    @ApiModelProperty("用户id")
    private String useid;

    @Column(updatable = false)
    @ApiModelProperty("完成时间")
    private Date entim = new Date();
}
