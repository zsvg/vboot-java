package vboot.core.module.sys.org.role;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.PageData;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.pojo.TreeMovePo;
import vboot.core.common.mvc.pojo.Ztree;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("sys/org/rnode")
public class SysOrgRoleNodeApi {

    private String table = "sys_org_role_node";

    @GetMapping
    public R get(String treid,String name, String pid, String notes) {
        if(StrUtil.isBlank(treid)){
            return R.ok(new PageData(0, new ArrayList<>()));
        }
        Sqler sqler = new Sqler(table);
        if (StrUtil.isNotBlank(name)) {
            sqler.addLike("t.name" , name);
        } else {
            if ("".equals(pid)) {
                sqler.addWhere("t.pid is null");
            } else if (StrUtil.isNotBlank(pid)) {
                sqler.addEqual("t.pid" , pid);
            }
        }
        sqler.addLike("t.notes" , notes);
        sqler.addWhere("t.avtag=1");
        sqler.addOrder("t.ornum");
        sqler.addEqual("t.treid",treid);
        sqler.addSelect("t.ornum,t.notes,t.pid,t.crtim,t.uptim");
        return R.ok(jdbcDao.findPageData(sqler));
    }

    //查询层级树
    @GetMapping("treea")
    public R getTreea(String treid) {
        Sqler sqler = new Sqler("sys_org_role_node");
        sqler.addSelect("t.pid");
        sqler.addEqual("t.treid", treid);
        sqler.addOrder("t.ornum");
        sqler.addWhere("t.avtag=1");
        List<Ztree> list = jdbcDao.findTreeList(sqler);
        return R.ok(list);
    }

    //查询层级树
    @GetMapping("tree")
    public R getTree(String name,String treid) {
        Sqler sqler = new Sqler("sys_org_role_node");
        sqler.addSelect("t.pid");
        sqler.addLike("t.name", name);
        sqler.addEqual("t.treid", treid);
        sqler.addOrder("t.ornum");
        sqler.addWhere("t.avtag=1");
        List<Ztree> list = jdbcDao.findTreeList(sqler);
        return R.ok(list);
    }

    //排除了自己
//    @GetMapping("listn")
//    public R listn(String name, String id) {
//        Sqler sqler = new Sqler(table);
//        sqler.addLike("t.name", name);
//        sqler.addWhere("t.avtag=1");
//        sqler.addOrder("t.ornum");
//        sqler.addSelect("t.pid");
//        return R.ok(service.findWithoutItself(sqler, id));
//    }

    @GetMapping("one/{id}")
    public R getOne(@PathVariable String id, HttpServletRequest request) {
        SysOrgRoleNode main = service.findById(id);
        return R.ok(main);
    }

    @PostMapping
    public R post(@RequestBody SysOrgRoleNode main) {
        main.setOrnum(service.getCount(main.getPid(),main.getTreid())+1);
        if(StrUtil.isNotBlank(main.getPid())){
            main.setParent(new SysOrgRoleNode(main.getPid()));
        }
        service.insert(main);
        return R.ok(main.getId());
    }

    @PutMapping
    public R put(@RequestBody SysOrgRoleNode main) throws Exception {
        if(StrUtil.isNotBlank(main.getPid())){
            main.setParent(new SysOrgRoleNode(main.getPid()));
        }
        service.update(main);
        return R.ok(main.getId());
    }

    @DeleteMapping("{ids}")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @PostMapping("move")
    public R move(@RequestBody TreeMovePo po) throws Exception {
        service.move(po);
        return R.ok();
    }


    @Autowired
    private JdbcDao jdbcDao;

    @Autowired
    private SysOrgRoleNodeService service;

}
