package vboot.core.module.sys.portal.main;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseMainEntity;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ApiModel("门户信息")
public class SysPortalMain extends BaseMainEntity {

    @ApiModelProperty("排序号")
    private Integer ornum;

//    @ManyToMany
//    @JoinTable(name = "sys_portal_viewer", joinColumns = {@JoinColumn(name = "mid")},
//            inverseJoinColumns = {@JoinColumn(name = "oid")})
//    private List<SysOrg> viewers;
}