package vboot.core.module.sys.org.role;


import org.springframework.data.jpa.repository.JpaRepository;

public interface SysOrgRoleTreeRepo extends JpaRepository<SysOrgRoleTree,String> {


}

