package vboot.core.module.sys.org.dept;

import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.pojo.Ztree;

import java.util.Date;

@Getter
@Setter
public class SysOrgDeptVo extends Ztree {

    private Date crtim;

    private Date uptim;

    private String notes;

    private Integer ornum;

}
