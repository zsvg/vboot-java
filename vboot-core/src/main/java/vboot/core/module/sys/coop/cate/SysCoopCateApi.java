package vboot.core.module.sys.coop.cate;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.pojo.Ztree;

import java.util.List;

@RestController
@RequestMapping("sys/coop/cate")
public class SysCoopCateApi {

    private String table = "sys_coop_cate";

    @GetMapping("treez")
    @ApiOperation("查询分类除自身外的分类树")
    public R getTreez(String name, String id) {
        List<Ztree> list = service.tier2Choose(table, id, name);
        return R.ok(list);
    }

    @GetMapping("treea")
    @ApiOperation("查询分类树")
    public R getTreea(String name) {
        List<Ztree> list = service.findTreeList(table, name);
        return R.ok(list);
    }

    @GetMapping("tree")
    @ApiOperation("查询分类列表")
    public R getTree(String name) {
        Sqler sqler = new Sqler(table);
        sqler.addLike("t.name", name);
        sqler.addOrder("t.ornum");
        sqler.addWhere("t.avtag=1");
        sqler.addSelect("t.crtim,t.uptim,t.pid,t.notes");
        List<SysCoopCate> list = service.findTree(sqler);
        return R.ok(list);
    }

    @GetMapping
    @ApiOperation("查询分类分页")
    public R get(String name) {
        Sqler sqler = new Sqler(table);
        sqler.addLike("t.name", name);
        sqler.addSelect("t.notes,t.ornum");
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询分类详情")
    public R getOne(@PathVariable String id) {
        SysCoopCate cate = service.findOne(id);
        return R.ok(cate);
    }

    @PostMapping
    @ApiOperation("新增分类")
    public R post(@RequestBody SysCoopCate cate) {
        return R.ok(service.insertx(cate));
    }

    @PutMapping
    @ApiOperation("更新分类")
    public R put(@RequestBody SysCoopCate cate) {
        return R.ok(service.updatex(cate));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除分类")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.deletex(ids));
    }

    @Autowired
    private SysCoopCateService service;

}