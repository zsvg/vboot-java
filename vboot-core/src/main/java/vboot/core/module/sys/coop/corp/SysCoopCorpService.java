package vboot.core.module.sys.coop.corp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vboot.core.common.mvc.pojo.ZidName;
import vboot.core.common.mvc.service.BaseMainService;
import vboot.core.module.sys.coop.cate.SysCoopCate;
import vboot.core.module.sys.coop.cate.SysCoopCateRepo;
import vboot.core.module.sys.org.root.SysOrg;
import vboot.core.module.sys.org.root.SysOrgRepo;
import vboot.core.common.mvc.pojo.Ztree;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class SysCoopCorpService extends BaseMainService<SysCoopCorp> {

    public String insertx(SysCoopCorp corp){
        String id= insert(corp);
        SysOrg sysOrg=new SysOrg(id,corp.getName(),128);
        orgRepo.save(sysOrg);
        return id;
    }

    public String updatex(SysCoopCorp corp){
        SysCoopCorp oldCorp= repo.getOne(corp.getId());
        SysCoopCate oldCate= cateRepo.getOne(oldCorp.getCatid());
        String oldTier=oldCate.getTier();
        SysCoopCate newCate= cateRepo.getOne(corp.getCatid());
        update(corp);
        SysOrg sysOrg=new SysOrg(corp.getId(),corp.getName(),128);
        orgRepo.save(sysOrg);
        if(!newCate.getTier().equals(oldTier)){
            dealUserTier(oldTier+corp.getId()+"x",newCate.getTier()+corp.getId()+"x");
        }
        return corp.getId();
    }

    private void dealUserTier(String oldTier, String newTier) {
        String sql = "select id,tier as name from sys_coop_user where tier like ?";
        List<ZidName> list = jdbcDao.findIdNameList(sql, oldTier + "%");
        String updateSql = "update sys_coop_user set tier=? where id=?";
        List<Object[]> updateList = new ArrayList<>();
        batchReady(oldTier, newTier, list, updateList);
        jdbcDao.batch(updateSql, updateList);
    }

    private void batchReady(String oldTier, String newTier, List<ZidName> list, List<Object[]> updateList) {
        for (ZidName ztwo : list) {
            Object[] arr = new Object[2];
            arr[1] = ztwo.getId();
            arr[0] = ztwo.getName().replace(oldTier, newTier);
            updateList.add(arr);
        }
    }


    public int deletex(String[] ids) {
        for (String id : ids) {
            repo.deleteById(id);
            orgRepo.deleteById(id);
        }
        return ids.length;
    }

    public List<Ztree> findTreeList() {
//        Sqler cateSqler = new Sqler("sys_coop_cate");
//        cateSqler.addSelect("pid,'' type");
//        cateSqler.addOrder("t.ornum");
//        List<Ztree> list = jdbcDao.getTp().query(cateSqler.getSql(),cateSqler.getParams(), new BeanPropertyRowMapper<>(Ztree.class));
//        Sqler mainSqler = new Sqler("sys_coop_corp");
//        mainSqler.addSelect("t.catid as pid");
//        mainSqler.addLike("t.name", name);
//        mainSqler.addOrder("t.ornum");
//        List<Ztree> list2 = jdbcDao.getTp().query(mainSqler.getSql(),mainSqler.getParams(), new BeanPropertyRowMapper<>(Ztree.class));
//        list.addAll(list2);
//        return TreeUtils.build(list);
        String sql = "select id,pid,name,'cate' type from sys_coop_cate " +
                "union all select id,catid as pid,name,'corp' type from sys_coop_corp";
        return jdbcDao.findTreeList(sql);
    }

    //----------bean注入------------
    @Autowired
    private SysOrgRepo orgRepo;

    @Autowired
    private SysCoopCateRepo cateRepo;

    @Autowired
    private SysCoopCorpRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }

}
