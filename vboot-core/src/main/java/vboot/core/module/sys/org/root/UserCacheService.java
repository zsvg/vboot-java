package vboot.core.module.sys.org.root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vboot.core.common.mvc.dao.JdbcDao;

@Service
@Transactional(rollbackFor = Exception.class)
public class UserCacheService {

    public void reset(){
        jdbcDao.update("update sys_org_user set catag = 0");
        jdbcDao.update("update sys_coop_user set catag = 0");
    }

    @Autowired
    private JdbcDao jdbcDao;

}
