package vboot.web.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vboot.core.module.ass.dict.data.AssDictData;
import vboot.core.module.ass.dict.data.AssDictDataRepo;
import vboot.core.module.ass.dict.main.AssDictMain;
import vboot.core.module.ass.dict.main.AssDictMainRepo;
import vboot.core.module.ass.num.main.AssNumMain;
import vboot.core.module.ass.num.main.AssNumMainRepo;
import vboot.core.common.utils.lang.IdUtils;

import java.util.ArrayList;
import java.util.List;

//辅助数据初始化
@Component
public class AssInit {

    protected void initData() throws Exception {
        initNum();
        initDict();
    }

    //流水号初始化
    private void initNum() {
        List<AssNumMain> numList = new ArrayList<>();
        AssNumMain suppNum = new AssNumMain();
        suppNum.setId("DEMO");
        suppNum.setName("DEMO流水号");
        suppNum.setNumod("yy");
        suppNum.setNulen(4);
        suppNum.setNflag(true);
        suppNum.setNupre("D");
        numList.add(suppNum);

        AssNumMain oaProjNum = new AssNumMain();
        oaProjNum.setId("OAPROJ");
        oaProjNum.setName("OA项目流水号");
        oaProjNum.setNumod("yyyymmdd");
        oaProjNum.setNulen(3);
        oaProjNum.setNflag(true);
        oaProjNum.setNupre("OAPROJ");
        numList.add(oaProjNum);

        AssNumMain projNum = new AssNumMain();
        projNum.setId("PROJ");
        projNum.setName("CRM项目流水号");
        projNum.setNumod("yyyymmdd");
        projNum.setNulen(3);
        projNum.setNflag(true);
        projNum.setNupre("P");
        numList.add(projNum);

        AssNumMain custNum = new AssNumMain();
        custNum.setId("CUST");
        custNum.setName("CRM客户流水号");
        custNum.setNumod("yyyymmdd");
        custNum.setNulen(3);
        custNum.setNflag(true);
        custNum.setNupre("C");
        numList.add(custNum);

        AssNumMain distNum = new AssNumMain();
        distNum.setId("DIST");
        distNum.setName("CRM渠道商流水号");
        distNum.setNumod("yyyymmdd");
        distNum.setNulen(3);
        distNum.setNflag(true);
        distNum.setNupre("D");
        numList.add(distNum);
        numRepo.saveAll(numList);

    }

    //数据字典初始化
    private void initDict() {
        List<AssDictMain> dictList = new ArrayList<>();

        AssDictMain demoDict = new AssDictMain();
        demoDict.setId("DEMO_GRADE");
        demoDict.setName("DEMO等级");
        demoDict.setAvtag(true);
        dictList.add(demoDict);

        AssDictMain distDict = new AssDictMain();
        distDict.setId("DIST_GRADE");
        distDict.setName("渠道商资质等级");
        distDict.setAvtag(true);
        dictList.add(distDict);

        dictRepo.saveAll(dictList);

        List<AssDictData> dictDataList = new ArrayList<>();
        AssDictData demo1 = new AssDictData();
        demo1.setId(IdUtils.getUID());
        demo1.setCode("A");
        demo1.setName("等级A");
        demo1.setAvtag(true);
        demo1.setOrnum(1);
        demo1.setMaiid("DEMO_GRADE");
        dictDataList.add(demo1);

        AssDictData demo2 = new AssDictData();
        demo2.setId(IdUtils.getUID());
        demo2.setCode("B");
        demo2.setName("等级B");
        demo2.setAvtag(true);
        demo2.setOrnum(2);
        demo2.setMaiid("DEMO_GRADE");
        dictDataList.add(demo2);

        AssDictData demo3 = new AssDictData();
        demo3.setId(IdUtils.getUID());
        demo3.setCode("C");
        demo3.setName("等级C");
        demo3.setAvtag(true);
        demo3.setOrnum(3);
        demo3.setMaiid("DEMO_GRADE");
        dictDataList.add(demo3);

        AssDictData demo4 = new AssDictData();
        demo4.setId(IdUtils.getUID());
        demo4.setCode("Z");
        demo4.setName("不合格");
        demo4.setAvtag(true);
        demo4.setOrnum(4);
        demo4.setMaiid("DEMO_GRADE");
        dictDataList.add(demo4);

        AssDictData dist1 = new AssDictData();
        dist1.setId(IdUtils.getUID());
        dist1.setCode("A");
        dist1.setName("A级资质");
        dist1.setAvtag(true);
        dist1.setOrnum(1);
        dist1.setMaiid("DIST_GRADE");
        dictDataList.add(dist1);

        AssDictData dist2 = new AssDictData();
        dist2.setId(IdUtils.getUID());
        dist2.setCode("B");
        dist2.setName("B级资质");
        dist2.setAvtag(true);
        dist2.setOrnum(2);
        dist2.setMaiid("DIST_GRADE");
        dictDataList.add(dist2);

        AssDictData dist3 = new AssDictData();
        dist3.setId(IdUtils.getUID());
        dist3.setCode("C");
        dist3.setName("C级资质");
        dist3.setAvtag(true);
        dist3.setOrnum(3);
        dist3.setMaiid("DIST_GRADE");
        dictDataList.add(dist3);

        AssDictData dist4 = new AssDictData();
        dist4.setId(IdUtils.getUID());
        dist4.setCode("Z");
        dist4.setName("不合格");
        dist4.setAvtag(true);
        dist4.setOrnum(4);
        dist4.setMaiid("DIST_GRADE");
        dictDataList.add(dist4);

        dictDataRepo.saveAll(dictDataList);
    }

    @Autowired
    private AssDictMainRepo dictRepo;

    @Autowired
    private AssDictDataRepo dictDataRepo;

    @Autowired
    private AssNumMainRepo numRepo;
}
