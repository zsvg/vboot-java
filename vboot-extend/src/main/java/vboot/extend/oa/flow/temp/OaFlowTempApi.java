package vboot.extend.oa.flow.temp;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.pojo.Ztree;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("oa/flow/temp")
@Api(tags = {"OA办公-流程模板"})
public class OaFlowTempApi {

    @GetMapping
    @ApiOperation("查询流程模板分页")
    public R get(String name,String catid) {
        Sqler sqler = new Sqler("oa_flow_temp");
        sqler.addInnerJoin("c.name catna", "oa_flow_cate c", "c.id=t.catid");
        if(StrUtil.isNotBlank(name)){
            sqler.addLike("t.name", name);
        }else if(StrUtil.isNotBlank(catid)){
            sqler.addEqual("t.catid", catid);
        }
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("tree")
    @ApiOperation("查询流程模板树")
    public R getTree() {
        List<Ztree> list = service.findTreeList();
        return R.ok(list);
    }

    @GetMapping("list")
    @ApiOperation("查询流程模板列表")
    public R getList(String catid, String name) {
        Sqler sqler = new Sqler("oa_flow_temp");
        sqler.addInnerJoin("c.name catna", "oa_flow_cate c", "c.id=t.catid");
        sqler.addLike("t.name", name);
        sqler.addEqual("t.catid", catid);
        sqler.addOrder("t.ornum");
        return R.ok(jdbcDao.findMapList(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询流程模板详情")
    public R getOne(@PathVariable String id) {
        OaFlowTemp main = service.findOne(id);
        String sql = "select orxml from bpm_proc_temp where id=?";
        Map<String, Object> map = jdbcDao.findMap(sql, main.getProtd());
        main.setPrxml((String)map.get("orxml"));
        return R.ok(main);
    }

    @PostMapping
    @ApiOperation("新增流程模板")
    public R post(@RequestBody OaFlowTemp main) throws DocumentException {
        return R.ok(service.insertx(main));
    }

    @PutMapping
    @ApiOperation("修改流程模板")
    public R put(@RequestBody OaFlowTemp main) {
        return R.ok(service.updatex(main));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除流程模板")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private JdbcDao jdbcDao;

    @Autowired
    private OaFlowTempService service;

}
