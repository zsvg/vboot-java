package vboot.extend.oa.flow.temp;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import vboot.core.common.mvc.service.BaseMainService;
import vboot.core.common.utils.lang.IdUtils;
import vboot.core.module.bpm.proc.temp.BpmProcTemp;
import vboot.core.module.bpm.proc.temp.BpmProcTempService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vboot.core.common.utils.lang.TreeUtils;
import vboot.core.common.mvc.pojo.Ztree;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class OaFlowTempService extends BaseMainService<OaFlowTemp> {

    public String insertx(OaFlowTemp oaFlowTemp) {
        BpmProcTemp bpmProcTemp = new BpmProcTemp(oaFlowTemp);
        bpmProcTemp.setOrxml(oaFlowTemp.getPrxml());
        String chxml = "<?xml version=\"1.0\" encoding=\"gb2312\"?>"
                + "\n<process" + bpmProcTemp.getOrxml().split("bpmn2:process")[1]
                .replaceAll("bpmn2:", "")
                .replaceAll("activiti:", "") + "process>";
        bpmProcTemp.setChxml(chxml);
        String temid = bpmProcTempService.insert(bpmProcTemp);
        oaFlowTemp.setProtd(temid);
        return super.insert(oaFlowTemp);
    }

//    public String updatex(OaFlowTempl oaFlowTemp) {
//        BpmProcTemp bpmProcTemp = bpmProcTempService.findOne(oaFlowTemp.getProtd());
//        bpmProcTemp.setOrxml(oaFlowTemp.getPrxml());
//        String chxml = "<?xml version=\"1.0\" encoding=\"gb2312\"?>"
//                + "\n<process" + bpmProcTemp.getOrxml().split("bpmn2:process")[1]
//                .replaceAll("bpmn2:", "")
//                .replaceAll("activiti:", "") + "process>";
//        bpmProcTemp.setChxml(chxml);
//        bpmProcTempService.update(bpmProcTemp);
//        return super.update(oaFlowTemp);
//    }

    public String updatex(OaFlowTemp oaFlowTemp) {
        BpmProcTemp bpmProcTemp = bpmProcTempService.findOne(oaFlowTemp.getProtd());
        if(!bpmProcTemp.getOrxml().equals(oaFlowTemp.getPrxml())){
            String newid= IdUtils.getUID();
            oaFlowTemp.setProtd(newid);
            BpmProcTemp newBpmProcTemp=new BpmProcTemp();
            newBpmProcTemp.setId(newid);
            newBpmProcTemp.setOrxml(oaFlowTemp.getPrxml());
            String chxml = "<?xml version=\"1.0\" encoding=\"gb2312\"?>"
                    + "\n<process" + newBpmProcTemp.getOrxml().split("bpmn2:process")[1]
                    .replaceAll("bpmn2:", "")
                    .replaceAll("activiti:", "") + "process>";
            newBpmProcTemp.setChxml(chxml);
            bpmProcTempService.insert(newBpmProcTemp);
        }
        return super.update(oaFlowTemp);
    }

    public List<Ztree> findTreeList() {
        String sql1 = "select id,name,pid from oa_flow_cate";
        List<Ztree> list1 = jdbcDao.getTp().query(sql1,new BeanPropertyRowMapper<>(Ztree.class));
        String sql2 = "select id,name,catid pid from oa_flow_temp";
        List<Ztree> list2 = jdbcDao.getTp().query(sql2,new BeanPropertyRowMapper<>(Ztree.class));
        list1.addAll(list2);
        return TreeUtils.build(list1);
    }

    //----------bean注入------------
    @Autowired
    private BpmProcTempService bpmProcTempService;

    @Autowired
    private OaFlowTempRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }


}
