package vboot.extend.oa.flow.rece;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.utils.web.XuserUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("oa/flow/rece")
@Api(tags = {"OA办公-流程使用记录"})
public class OaFlowReceApi {

    @GetMapping("list")
    @ApiOperation("查询流程使用记录列表")
    public R getList(Integer type) {
        List<Map<String, Object>> mapList = new ArrayList<>();
        String userId = XuserUtil.getUserId();
        Sqler sqler = new Sqler("t.floid as id", "oa_flow_rece");
        sqler.addInnerJoin("u.name", "oa_flow_temp u", "u.id=t.floid");
        sqler.addInnerJoin("c.name as canam", "oa_flow_cate c", "c.id=u.catid");
        sqler.addEqual("t.useid", userId);
        sqler.addOrder("t.uptim desc");
        mapList.addAll(jdbcDao.findMapList(sqler));
        return R.ok(mapList);
    }

    @PostMapping
    @ApiOperation("更新流程使用记录")
    public R post(@RequestBody List<OaFlowRece> reces) throws SQLException {
        if (reces != null && reces.size() > 0) {
            service.update(reces);
        }
        return R.ok();
    }


    @Autowired
    private OaFlowReceService service;

    @Autowired
    private JdbcDao jdbcDao;


}
