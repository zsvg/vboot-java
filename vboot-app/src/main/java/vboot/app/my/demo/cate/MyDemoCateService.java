package vboot.app.my.demo.cate;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Service;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.service.BaseCateService;
import vboot.core.module.bpm.proc.temp.BpmProcTemp;
import vboot.core.module.bpm.proc.temp.BpmProcTempService;

import javax.annotation.PostConstruct;
import java.util.List;

//DEMO分类服务
@Service
public class MyDemoCateService extends BaseCateService<MyDemoCate> {

    //获取分类treeTable数据
    public List<MyDemoCate> findTree(Sqler sqler) {
        List<MyDemoCate> list = jdbcDao.getTp().query(sqler.getSql(), sqler.getParams(),
                new BeanPropertyRowMapper<>(MyDemoCate.class));
        return buildByRecursive(list);
    }

    public String insertx(MyDemoCate cate) {
        BpmProcTemp bpmProcTemp =  new BpmProcTemp();
        bpmProcTemp.setName(cate.getName());
        bpmProcTemp.setOrxml(cate.getPrxml());
        if(StrUtil.isNotBlank((bpmProcTemp.getOrxml()))){
            String chxml = "<?xml version=\"1.0\" encoding=\"gb2312\"?>"
                    + "\n<process" + bpmProcTemp.getOrxml().split("bpmn2:process")[1]
                    .replaceAll("bpmn2:", "")
                    .replaceAll("activiti:", "") + "process>";
            bpmProcTemp.setChxml(chxml);
        }
        String temid = bpmProcTempService.insert(bpmProcTemp);
        cate.setProtd(temid);
        return super.insert(cate);
    }

    public String updatex(MyDemoCate cate,String table) {
        BpmProcTemp bpmProcTemp = bpmProcTempService.findOne(cate.getProtd());
        bpmProcTemp.setName(cate.getName());
        bpmProcTemp.setOrxml(cate.getPrxml());
        if(StrUtil.isNotBlank((bpmProcTemp.getOrxml()))) {
            String chxml = "<?xml version=\"1.0\" encoding=\"gb2312\"?>"
                    + "\n<process" + bpmProcTemp.getOrxml().split("bpmn2:process")[1]
                    .replaceAll("bpmn2:", "")
                    .replaceAll("activiti:", "") + "process>";
            bpmProcTemp.setChxml(chxml);
        }
        bpmProcTempService.update(bpmProcTemp);
        return super.update(cate,table,false);
    }

    @Autowired
    private BpmProcTempService bpmProcTempService;

    @Autowired
    private MyDemoCateRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }

}
