package vboot.app.my.demo.cate;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.pojo.Ztree;
import vboot.core.common.utils.lang.TreeUtils;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("my/demo/cate")
@Api(tags = {"DEMO分类管理"})
public class MyDemoCateApi {

    private String table = "my_demo_cate";

    @GetMapping("treez")
    @ApiOperation("查询DEMO除自身外的分类树")
    public R getTreez(String name, String id) {
        List<Ztree> list = service.tier2Choose(table, id, name);
        return R.ok(list);
    }

    @GetMapping("treea")
    @ApiOperation("查询DEMO分类树")
    public R getTreea(String name) {
        Sqler sqler=new Sqler(table);
        sqler.addSelect("t.pid,t.fomod type");
        sqler.addOrder("t.ornum");
        sqler.addWhere("t.avtag=1");
        List<Ztree> list = jdbcDao.getTp().query(sqler.getSql(),sqler.getParams(),
                new BeanPropertyRowMapper<>(Ztree.class));
        return R.ok(TreeUtils.build(list)) ;
    }

    @GetMapping("tree")
    @ApiOperation("查询DEMO分类列表")
    public R getTree(String name) {
        Sqler sqler = new Sqler(table);
        sqler.addLike("t.name", name);
        sqler.addOrder("t.ornum");
        sqler.addWhere("t.avtag=1");
        sqler.addSelect("t.pid,t.notes");
        sqler.selectCUinfo();
        List<MyDemoCateVo> list = jdbcDao.getTp().query(sqler.getSql(), sqler.getParams(),
                new BeanPropertyRowMapper<>(MyDemoCateVo.class));
        return R.ok(TreeUtils.buildEtree(list));
    }

    @GetMapping
    @ApiOperation("查询DEMO分类分页")
    public R get(String name) {
        Sqler sqler = new Sqler(table);
        sqler.addLike("t.name", name);
        sqler.addSelect("t.notes");
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询DEMO分类详情")
    public R getOne(@PathVariable String id) {
        MyDemoCate cate = service.findOne(id);
        if(cate.getPrtag()!=null&&cate.getPrtag()){
            String sql = "select orxml from bpm_proc_temp where id=?";
            Map<String, Object> map = jdbcDao.findMap(sql, cate.getProtd());
            cate.setPrxml((String)map.get("orxml"));
        }
        return R.ok(cate);
    }

    @PostMapping
    @ApiOperation("新增DEMO分类")
    public R post(@RequestBody MyDemoCate main) {
        return R.ok(service.insertx(main));
    }

    @PutMapping
    @ApiOperation("更新DEMO分类")
    public R put(@RequestBody MyDemoCate main) {
        return R.ok(service.updatex(main, table));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除DEMO分类")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private MyDemoCateService service;

    @Autowired
    private JdbcDao jdbcDao;

}
